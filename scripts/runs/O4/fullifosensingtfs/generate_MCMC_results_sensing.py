import numpy as np
import os
import pydarm
import argparse
from scipy import signal

# Point to where all the calibration files live
os.environ['CAL_DATA_ROOT'] = '/ligo/svncommon/CalSVN/aligocalibration/trunk/'
directory = 'results'

#  ============================ Setting up the argparser ===============================
parser = argparse.ArgumentParser()

# Required arguments

parser.add_argument("--model_file", help="Model file")
parser.add_argument("--clg_measurement", help="XML file for CLG measurement", type=pydarm.measurement.Measurement)
parser.add_argument("--pcal_to_darm", help="XML file for PCAL to DARM measurement", type=pydarm.measurement.Measurement)
parser.add_argument("--clg_channels", help="Channels for the CG measurement", nargs=2)
parser.add_argument("--pcal_to_darm_channels", help="Channels for the PCAL to DARM measurement", nargs=2)

# Optional arguments
parser.add_argument("--fmin", help="Minimum MCMC frequency", type=int, default=10)
parser.add_argument("--fmax", help="Maximimum MCMC frequency", type=int, default=1000)
parser.add_argument("--burn_in_steps", help="Steps for burn in", type=int, default=200)
parser.add_argument("--steps", help="Steps for MCMC sampling", type=int, default=200)

args = parser.parse_args()

# getting the measurement date
measfilename = args.clg_measurement.filename.split('/')[-1]
date = measfilename.split('_')[0]
time = measfilename.split('_')[1]

cohThresh = 0.9

# Running & saving the MCMC
meas = pydarm.measurement.ProcessSensingMeasurement(args.model_file, args.clg_measurement, args.pcal_to_darm,
                                                    args.clg_channels, args.pcal_to_darm_channels,
                                                    meas1_cohThresh=cohThresh, meas2_cohThresh=cohThresh)


filename = f'{date}_{time}_sensing_MCMC.json'
filename_chain = f'{date}_{time}_sensing_MCMC_chain.hdf5'

chain = meas.run_mcmc(fmin=args.fmin, fmax=args.fmax, 
                      burn_in_steps=args.burn_in_steps, steps=args.steps,
                      save_map_to_file=f'{directory}/{filename}',
                      save_chain_to_file=f'{directory}/{filename_chain}')

MAP = np.median(chain, axis=0)
print(f'sensing MCMC MAP values: {MAP}')

#  ============================ Plots ===============================
    
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
mpl.rcParams.update({'text.usetex': True,
                     'axes.linewidth': 1,
                     'axes.grid': True,
                     'axes.labelweight': 'normal',
                     'font.family': 'DejaVu Sans',
                     'font.size': 20})
from matplotlib import ticker

frange_plot = [5, 2000]
frequencies, C_tf, C_unc = meas.get_processed_measurement_response()
darm = pydarm.darm.DARMModel(args.model_file)
fit_tf = MAP[0]*signal.freqresp(
    darm.sensing.optical_response(MAP[1],MAP[2],MAP[3],darm.sensing.is_pro_spring),
    2*np.pi*frequencies)[1]

fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(15, 12))
ax0, ax1 = axes.flat

ax0.plot(frequencies, abs(fit_tf),label='MAP',color='magenta')
ax0.errorbar(frequencies,np.abs(C_tf), marker='o', markersize=10, 
             linestyle='', yerr=C_unc*np.abs(C_tf), label='Meas.')
ax0.set_xlim(frange_plot)
ax0.legend(loc='lower center', ncol=2, handlelength=2)
ax0.set_ylabel(r'Mag (ct/m)')

ax1.plot(frequencies, np.angle(fit_tf,deg=True),color='magenta')
ax1.errorbar(frequencies,np.angle(C_tf)*180.0/np.pi,marker='o', markersize=10, linestyle='', yerr=C_unc*180.0/np.pi)
ax1.set_xlim(frange_plot)
ax1.set_xlabel(r'Frequency (Hz)')
ax1.set_ylabel(r'Phase (deg)')

for ax in axes.flat:
    ax.grid(which='major',color='black')
    ax.grid(which='minor',ls='--')
    ax.set_xscale('log')
    ax.axvline(args.fmin,ls='--',color='C06')
    ax.axvline(args.fmax,ls='--',color='C06')

plt.savefig(f'{directory}/{date}_{time}_sensing_MCMC_plot.pdf', bbox_inches='tight', pad_inches=0.2)

python3 generate_MCMC_results_sensing.py \
 --model_file /ligo/groups/cal/ifo/L1/pydarm_L1.ini \
 --clg_measurement /ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/L1/Measurements/FullIFOSensingTFs/2023-01-20a/2023-01-20_L1_DARM_OLGTF_5to1000Hz.xml \
 --pcal_to_darm /ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/L1/Measurements/PCAL/2023-01-20a/2023-01-20_L1_PCALY2DARMTF_5to1000Hz_OLG.xml \
 --clg_channels L1:LSC-DARM_IN2 L1:LSC-DARM_EXC --pcal_to_darm_channels L1:CAL-PCALY_RX_PD_OUT L1:LSC-DARM_IN1 \
 --fmin 10 --fmax 1200 --burn_in_steps 100 --steps 900


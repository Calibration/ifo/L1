from matplotlib.patches import Patch as mplpatches
from matplotlib.lines import Line2D as box
from matplotlib import ticker as tck
from matplotlib import pyplot as plt
from datetime import datetime as dt
from collections import namedtuple
from scipy import signal
import numpy as np
import pydarm
import corner

# Mak'a'da plots look pretty, courtesy of Daniel Brown
plt.rcParams.update({'text.usetex': False,
                    'lines.linewidth': 3,
                    'font.family': 'sans-serif',
                    'font.serif': 'Helvetica',
                    'font.size': 8,
                    'xtick.labelsize': 'x-large',
                    'ytick.labelsize': 'x-large',
                    'axes.labelsize': 'x-large',
                    'axes.titlesize': 'x-large',
                    'axes.grid': True,
                    'grid.alpha': 0.5,
                    'lines.markersize': 12,
                    'legend.borderpad': 0.2,
                    'legend.fancybox': True,
                    'legend.fontsize': 'large',
                    'legend.framealpha': 0.7,
                    'legend.handletextpad': 0.1,
                    'legend.labelspacing': 0.2,
                    'legend.loc': 'best',
                    'figure.figsize': (12,8),
                    'savefig.dpi': 100,
                    'pdf.compression': 9,
                    'pdf.fonttype': 42,
                    })

"""
Copied originally from
[1] https://git.ligo.org/Calibration/pydarm/-/blob/master/examples/example_process_optical_response.py
    But was *very* bare bones.
Got advice from Evan to use the ipython notebook that coming soon via merge request:
[2] https://git.ligo.org/evan-goetz/pydarm/-/blob/update-doc/examples/pydarm_example.ipynb
Took that, and successfull produced an answer, then started coping over all the information extraction stuff from
sensing_MCMC method in
[3] https://svn.ligo.caltech.edu/svn/aligocalibration/trunk/Common/pyDARM/src/sensing.py
"""

# Note that you may need to specify where the aligocalibration SVN repo
# base path is on your local checkout.
# no need for backslash at the end. doesn't matter.
cal_data_root = '/ligo/svncommon/CalSVN/aligocalibration/trunk'
cal_ifo_root = '/ligo/groups/cal/ifo/'

run = 'O3'
IFO = 'L1'
measDate = '2023-01-20'
refPCAL = 'PCALY_RX'
todayDate =  dt.today().strftime('%Y%m%d')

verbose = True
printFigs = True

COHTHRESH_DARMOLGTF = 0.9
COHTHRESH_PCALTF = COHTHRESH_DARMOLGTF # for now

# Default is 1e3, 9e3. That takes ~20 minutes on Jeff's laptop. No thanks.
MCMC_PARAMS_BURNINSTEPS = 100;
MCMC_PARAMS_STEPS = 900;
MCMC_PARAMS_FITREGION_HZ = [20,1.2e3]

# These are the xml files we want to get our data from.
darmolgtf_filename = \
    '{}/Runs/{}/{}/Measurements/FullIFOSensingTFs/{}a/{}_{}_DARM_OLGTF_5to1000Hz.xml'\
    .format(cal_data_root,\
        run,\
        IFO,\
        measDate,\
        measDate,\
        IFO)
pcaltf_filename = \
    '{}/Runs/{}/{}/Measurements/PCAL/{}a/{}_{}_PCALY2DARMTF_5to1000Hz_OLG.xml'\
    .format(cal_data_root,\
    run,\
    IFO,\
    measDate,\
    measDate,\
    IFO)
    
print(' ')
print(darmolgtf_filename)
print(' ')
print(' ')
print(pcaltf_filename)
print(' ')

# This is the model file that we need to use in order to process the data.
model_parameters_file = cal_ifo_root+IFO+'/pydarm_L1.ini'
modelDate = model_parameters_file.split('.')[0][-8:] # This, of course, very much relies on folks using the same naming format for model parameter files. *sigh*

resultsDir = \
    cal_data_root+'/Runs/O3/'+IFO+'/Results/FullIFOSensingTFs/'

anythingExtraInTheFigTag = ''

figTag ='sensingFunction_{}_proc{}_model{}_meas{}_MCMCfitfreqrange{}-{}Hz{}'.format(IFO,
    todayDate,
    modelDate,
    measDate.replace('-',''),
    '{:.1f}'.format(MCMC_PARAMS_FITREGION_HZ[0]).replace('.','p'),
    '{:.1f}'.format(MCMC_PARAMS_FITREGION_HZ[1]).replace('.','p'),
    anythingExtraInTheFigTag)

# Build the model:
C = pydarm.sensing.SensingModel(model_parameters_file)
A = pydarm.actuation.DARMActuationModel(model_parameters_file)
darm = pydarm.darm.DARMModel(model_parameters_file)

# ----------------------------------------------------------------------
# If you want to get the processed sensing data for input to the  MCMC
# process, do the following:
darmolgtf_obj = pydarm.measurement.Measurement(darmolgtf_filename)
pcaltf_obj  = pydarm.measurement.Measurement(pcaltf_filename)

darmolgtf_freq, darmolgtf_tf,  darmolgtf_coh, darmolgtf_unc = darmolgtf_obj.get_raw_tf('L1:LSC-DARM_IN2',\
                                                                                       'L1:LSC-DARM_EXC',\
                                                                                       COHTHRESH_DARMOLGTF)
pcaltf_freq, pcaltf_tf,  pcaltf_coh, pcaltf_unc = pcaltf_obj.get_raw_tf('L1:CAL-PCALY_RX_PD_OUT',\
                                                                        'L1:LSC-DARM_IN1',\
                                                                        COHTHRESH_PCALTF)
# Create a sensing function measurement object
process_sensing_obj = pydarm.measurement.ProcessSensingMeasurement(
    model_parameters_file, darmolgtf_obj, pcaltf_obj,
    ('L1:LSC-DARM_IN2', 'L1:LSC-DARM_EXC'),
    ('L1:CAL-PCALY_RX_PD_OUT', 'L1:LSC-DARM_IN1'),
    COHTHRESH_DARMOLGTF, COHTHRESH_PCALTF)

# Extract the processed sening function measurement, with C_res removed
frequencies, processed_optical_response_tf, processed_optical_response_unc = \
    process_sensing_obj.get_processed_measurement_response()
    
# Fit only the optical response to our sensing function model optical parameters
mcmc_posterior_chain_obj = process_sensing_obj.run_mcmc(fmin=MCMC_PARAMS_FITREGION_HZ[0],\
                                                        fmax=MCMC_PARAMS_FITREGION_HZ[1],\
                                                        burn_in_steps=MCMC_PARAMS_BURNINSTEPS,\
                                                        steps=MCMC_PARAMS_STEPS)
MAP = np.median(mcmc_posterior_chain_obj, axis=0)

#### END PRIMARY COMPUTATION

#### START "INFORMATION EXTRACTION AND VALIDATION FROM COMPUTATION"

ciQuantiles = np.array([0.16, 0.5, 0.84])

quantileValues = np.zeros((len(MAP),3))
for nMAP in range(0,len(MAP)):
    quantileValues[nMAP,:] = corner.quantile(mcmc_posterior_chain_obj[:,nMAP], ciQuantiles)
    quantileValues[nMAP,2] = quantileValues[nMAP,2]-quantileValues[nMAP,1];
    quantileValues[nMAP,0] = quantileValues[nMAP,1]-quantileValues[nMAP,0];

# In the example [2], Evan updates the model file, but
# We'll want to show *both* the "reference" model and the MCMC MAP model
MAP_opticalGain_ct_p_m = MAP[0]
MAP_cavityPoleFreq_Hz = MAP[1]
MAP_detunedSpringFreq_Hz = MAP[2]
MAP_detunedSpringQ = 1/MAP[3]
MAP_residualTimeDelay_sec = MAP[4]

print("is_pro_spring test")
print(str(darm.sensing.is_pro_spring))
print(str(C.is_pro_spring))

normalized_MAP_optical_response_tf = signal.freqresp(darm.sensing.optical_response(MAP_cavityPoleFreq_Hz,
                                                                                   MAP_detunedSpringFreq_Hz,
                                                                                   MAP_detunedSpringQ,
                                                                                   darm.sensing.is_pro_spring),
                                                     2*np.pi*frequencies)[1]

MAP_optical_response_tf = (MAP_opticalGain_ct_p_m *
                           normalized_MAP_optical_response_tf *
                           np.exp(-2*np.pi*1j*MAP_residualTimeDelay_sec*frequencies*1e-6))

normalized_model_optical_response_tf = signal.freqresp(darm.sensing.optical_response(C.coupled_cavity_pole_frequency,
                                                                                     C.detuned_spring_frequency,
                                                                                     C.detuned_spring_q,
                                                                                     C.is_pro_spring),
                                                       2*np.pi*frequencies)[1]
#H1 stuff
#model_optical_response_tf = (C.coupled_cavity_optical_gain *
#                             normalized_model_optical_response_tf)

#### SPIT ANSWER OUT TO COMMAND LINE FOR COPY AND PASTE TO ALOG

if verbose:
    print('Parameter                                | Quantiles (0.15, 0.50, 0.84)')
    print('---------------------------------------------------------------------')
    print('Optical gain, H_c (ct/m)                 | {:4.4g}, {:4.4g}, {:4.4g}'.format(quantileValues[0,1]-quantileValues[0,0], quantileValues[0,1], quantileValues[0,1]+quantileValues[0,2]))
    print('Cavity pole, f_cc (Hz)                   | {:4.4g}, {:4.4g}, {:4.4g}'.format(quantileValues[1,1]-quantileValues[1,0], quantileValues[1,1], quantileValues[1,1]+quantileValues[1,2]))
    print('Detuned SRC spring frequency, f_s (Hz)   | {:4.4g}, {:4.4g}, {:4.4g}'.format(quantileValues[2,1]-quantileValues[2,0], quantileValues[2,1], quantileValues[2,1]+quantileValues[2,2]))
    print('Detuned SRC spring quality factor, Q_s   | {:4.4g}, {:4.4g}, {:4.4g}'.format(1./(quantileValues[3,1]-quantileValues[3,0]), 1./(quantileValues[3,1]), 1./(quantileValues[3,1]+quantileValues[3,2])))
    print('Residual time delay, tau_c (usec)        | {:4.4g}, {:4.4g}, {:4.4g}'.format(quantileValues[4,1]-quantileValues[4,0], quantileValues[4,1], quantileValues[4,1]+quantileValues[4,2]))
    print('------------------------------- OR ----------------------------------')
    print('Optical gain, H_c (ct/m)                 | {:4.4g} (+{:4.4g},-{:4.4g}) or (+{:4.4g}%,-{:4.4g}%)'.format(quantileValues[0,1],\
                                                                                                                quantileValues[0,2],\
                                                                                                                quantileValues[0,0],\
                                                                                                                quantileValues[0,2]/quantileValues[0,1]*100,\
                                                                                                                quantileValues[0,0]/quantileValues[0,1]*100))
    print('Optical gain, H_c (mA/pm)                | {:4.4g} (+{:4.4g},-{:4.4g}) or (+{:4.4g}%,-{:4.4g}%)'.format(quantileValues[0,1]*C.omc_dcpd_output_to_darmin1_tf * 1e-12,\
                                                                                                                quantileValues[0,2]*C.omc_dcpd_output_to_darmin1_tf * 1e-12,\
                                                                                                                quantileValues[0,0]*C.omc_dcpd_output_to_darmin1_tf * 1e-12,\
                                                                                                                quantileValues[0,2]/quantileValues[0,1]*100,\
                                                                                                                quantileValues[0,0]/quantileValues[0,1]*100))
    print('Cavity pole, f_cc (Hz)                   | {:4.4g} (+{:4.4g},-{:4.4g}) or (+{:4.4g}%,-{:4.4g}%)'.format(quantileValues[1,1],\
                                                                                                                quantileValues[1,2],\
                                                                                                                quantileValues[1,0],\
                                                                                                                quantileValues[1,2]/quantileValues[1,1]*100,\
                                                                                                                quantileValues[1,0]/quantileValues[1,1]*100))
    print('Detuned SRC spring frequency, f_s (Hz)   | {:4.4g} (+{:4.4g},-{:4.4g}) or (+{:4.4g}%,-{:4.4g}%)'.format(quantileValues[2,1],\
                                                                                                                quantileValues[2,2],\
                                                                                                                quantileValues[2,0],\
                                                                                                                quantileValues[2,2]/quantileValues[2,1]*100,\
                                                                                                                quantileValues[2,0]/quantileValues[2,1]*100))
    print('Detuned SRC spring quality factor, Q_s   | {:4.4g} (+{:4.4g},-{:4.4g}) or (+{:4.4g}%,-{:4.4g}%)'.format(1./quantileValues[3,1],\
                                                                                                                  1./quantileValues[3,2],\
                                                                                                                  1./quantileValues[3,0],\
                                                                                                                  quantileValues[3,2]/quantileValues[3,1]*100,\
                                                                                                                  quantileValues[3,0]/quantileValues[3,1]*100))
    print('Residual time delay, tau_c (usec)        | {:4.4g} (+{:4.4g},-{:4.4g}) or (+{:4.4g}%,-{:4.4g}%)'.format(quantileValues[4,1],\
                                                                                                                quantileValues[4,2],\
                                                                                                                quantileValues[4,0],\
                                                                                                                quantileValues[4,2]/quantileValues[4,1]*100,\
                                                                                                                quantileValues[4,0]/quantileValues[4,1]*100))
    print('')

    print('TDCF Parameter | Value')
    print('---------------------------')
    print('kappa_c = {:1.6g}'.format(MAP[0] / C.coupled_cavity_optical_gain))
    print('f_cc = {:4.4g}'.format(MAP[1]))
    print('f_s = {:4.4g} #anti-spring'.format(MAP[2]))
    print('f_s = {:4.4g}j #spring'.format(MAP[2]))
    print('Q = {:4.4g}'.format(1./MAP[3]))
    print('')
    
#################
##### PLOTS #####
#################
fignames = namedtuple('fignames',['filename'])
allfigures = []
from collections import namedtuple
               
## Corner plot of MCMC results
histBins = 100 # Set delibrately to have each bins contain 1% of the PDF #FIXME -- this doesn't really work any more. Ethan?
tickFontSize = 12
titleFontSize = 15
shadingColor = 'C3'
theLegend = ['$1\\sigma$','$2\\sigma$','$3\\sigma$', 'MAP', '1D PDF bins: {}'.format(histBins)]
MAP_variableNames = [r'$H_C$', r'$f_{cc}$', r'$f_s$', r'$1/Q$', r'$\Delta \tau_C$']

# See discussion in https://corner.readthedocs.io/en/latest/pages/sigmas.html
               # 1-sigma,              2-sigma,               3-sigma
ci_levels = (1 - np.exp(-1. / 2.), 1 - np.exp(- 4. / 2.), 1 - np.exp(-9 / 2.))

cp = corner.corner(mcmc_posterior_chain_obj,
                   bins = histBins,
                   quantiles = [0.16, 0.84],
                   smooth = 2.0,
                   labels = MAP_variableNames,
                   legend_labels = theLegend,
                   verbose = False,
                   label_kwargs = {'fontsize': tickFontSize},
                   show_titles = True,
                   title_fmt = '.3e',
                   title_kwargs = {'fontsize': titleFontSize},
                   plot_datapoints = False,
                   plot_density = False,
                   fill_contours = True,
                   color = shadingColor,
                   hist_kwargs = {'density':True}, # Was 'normed':True, but normed is a depricated parameter for histograms in matplotlib... I think https://github.com/dirac-institute/CMNN_Photoz_Estimator/pull/17' found a workaround
                   use_math_text = True,
                   plot_contours = True,
                   levels = ci_levels,
                   max_n_ticks = 5,
                   truths = MAP,
                   truth_color = 'C0')
length_sides = np.arange(0,len(MAP),1)
histogram_indexes = (len(MAP)+1)*length_sides
N_samples = len(mcmc_posterior_chain_obj)

for idx, ax in enumerate(cp.get_axes()): # Loop through all the axes and set the fontsize of the tick parameters
    ax.grid(True)

    ax.yaxis.set_major_formatter(tck.ScalarFormatter())
    ax.xaxis.set_major_formatter(tck.ScalarFormatter())
    ax.ticklabel_format(axis='both',style='sci', useMathText=True, scilimits=(0,0))
    ax.tick_params(axis='both', labelsize=tickFontSize)
    ax.yaxis.offsetText.set_fontsize(tickFontSize)
    ax.xaxis.offsetText.set_fontsize(tickFontSize)

    if idx in histogram_indexes:
        ax2 = ax.twinx()
        ax2.tick_params(axis='both', labelsize=tickFontSize)
        ax2.ticklabel_format(axis='both', style='sci', useMathText=True, scilimits=(0, 0))
        ax2.yaxis.offsetText.set_fontsize(tickFontSize)
        ax2.xaxis.offsetText.set_fontsize(tickFontSize)
        ax2.set_ylim((ax.get_ylim()[0]/N_samples*100, ax.get_ylim()[1]/N_samples*100))
        ax2.set_xlim(ax.get_xlim())
        
        ax.yaxis.set_major_locator(plt.LinearLocator(5))
        ax2.yaxis.set_major_locator(plt.LinearLocator(5))
        
        ax2.yaxis.set_major_formatter(plt.FormatStrFormatter('%.1g'))
        ax2.set_ylabel('1D Norm. PDF \n (Percent per bin)',fontsize=titleFontSize)
        
        tick_labels = np.array(np.round(np.array(ax2.get_yticks(),dtype=float),2),dtype=str)
        tick_labels[0] = ''
        ax2.set_yticklabels(tick_labels)
    
    # Handling for if the subplot is not on the far-left side
    if idx not in len(MAP)*length_sides or idx == 0:
        ax.set_yticklabels([])
        ax.yaxis.offsetText.set_visible(False)
    if idx not in length_sides + len(MAP)*(len(MAP)-1):
        ax.set_xticklabels([])
        ax.xaxis.offsetText.set_visible(False)

# Generating the legend
alphas = np.ones(len(ci_levels)+1)
for i, l in enumerate(ci_levels):
        alphas[i] *= float(i) / (len(ci_levels)+1)
alphas = alphas[1:][::-1]

legend_symbols = [mplpatches(facecolor=shadingColor, edgecolor=shadingColor, label=theLegend[i], alpha=alphas[i]) for i in range(len(ci_levels))]
legend_symbols.append(box([0],[0], color='C9', lw=2))
legend_symbols.append(mplpatches(facecolor='C0', edgecolor='C0', label=theLegend[-1], alpha=0))

legend = cp.legend(legend_symbols, theLegend, fontsize=titleFontSize, title='2D PDF Contours')
plt.setp(legend.get_title(),fontsize=titleFontSize)

cp.suptitle(measDate + ' ' + IFO + ' Sensing Function: MCMC Corner Plot',fontsize=titleFontSize)
cp.subplots_adjust(top=0.92, wspace=0.15, hspace=0.15)

thisfig = fignames(resultsDir+figTag+'_mcmcModel_paramCornerPlot.pdf')
allfigures.append(thisfig)
if printFigs:
    cp.savefig(thisfig.filename,bbox_inches='tight')

         
# Alright, let's recreate where we stood at the end of O3.
# Pulled from sensing_MCMC method of
# /ligo/svncommon/CalSVN/aligocalibration/trunk/Common/pyDARM/src/sensing.py (rev. 11739, last changed rev 11055)
expscale = int(np.floor(np.log10(MAP[0])))

#MCMC Fit against data with residuals

plotFreqRange = [4, 2e3]

fig = plt.figure()
s1 = fig.add_subplot(221)
s2 = fig.add_subplot(223)
s3 = fig.add_subplot(222)
s4 = fig.add_subplot(224)

s1.plot(frequencies, abs(MAP_optical_response_tf)/10**expscale, label='MCMC Fit')
#s1.plot(frequencies, abs(model_optical_response_tf)/10**expscale,label='2021 Model (H_C = {:.3f} [mA/pm], f_cc = {:.1f} [Hz]) '.format(C.coupled_cavity_optical_gain * C.omc_dcpd_output_to_darmin1_tf * 1e-12, C.coupled_cavity_pole_frequency))
s1.errorbar(frequencies, abs(processed_optical_response_tf)/10**expscale, yerr=processed_optical_response_unc*abs(processed_optical_response_tf)/10**expscale, fmt='.', label='Measurement / C_R')
s1.axvline(MCMC_PARAMS_FITREGION_HZ[0], color='k',ls='--',label='Fit Range = [{:.1f}, {:.1f}] Hz'.format(MCMC_PARAMS_FITREGION_HZ[0],MCMC_PARAMS_FITREGION_HZ[1]))
s1.axvline(MCMC_PARAMS_FITREGION_HZ[1], color='k',ls='--')
s1.set_ylabel('Magnitude (ct/m) x 10$^{{{}}}$'.format(expscale),usetex=True)
s1.set_xscale('log')
s1.set_yscale('log')
s1.grid(which='major',color='black')
s1.grid(which='minor', ls='--')
s1.set_xlim(plotFreqRange)
s1.xaxis.set_major_locator(tck.LogLocator(base=10))
s1.set_ylim(0.5,10)
s1.yaxis.set_major_locator(tck.LogLocator(base=10))
s1.legend(ncol=1)
s1.set_title('$H_C$ = %.4g${}_{-%.3g}^{+%.3g}$ (ct/m)'%(MAP[0],quantileValues[0,0],quantileValues[0,2]),usetex=True)

s2.errorbar(frequencies, np.angle(MAP_optical_response_tf, deg=True))
#s2.plot(frequencies, np.angle(model_optical_response_tf,deg=True))
s2.errorbar(frequencies, np.angle(processed_optical_response_tf, deg=True), yerr=processed_optical_response_unc*180.0/np.pi, fmt='.',  label='meas.')
s2.axvline(MCMC_PARAMS_FITREGION_HZ[0], color='k',ls='--',label='Fit Range = [{:.1f}, {:.1f}] Hz'.format(MCMC_PARAMS_FITREGION_HZ[0],MCMC_PARAMS_FITREGION_HZ[1]))
s2.axvline(MCMC_PARAMS_FITREGION_HZ[1], color='k',ls='--')
s2.set_xlabel('Frequency (Hz)')
s2.set_ylabel('Phase (deg)')
s2.set_xscale('log')
s2.grid(which='major',color='black')
s2.grid(which='minor', ls='--')
s2.set_xlim(plotFreqRange)
s2.xaxis.set_major_locator(tck.LogLocator(base=10))
s2.set_ylim(-135,45)
s2.yaxis.set_major_locator(tck.MultipleLocator(15))
s2.yaxis.set_minor_locator(tck.MultipleLocator(5))
s2.set_title('$H_C$ = %.4g${}_{-%.3g}^{+%.3g}$ (mA/pm)'%(MAP[0] * C.omc_dcpd_output_to_darmin1_tf * 1e-12,
                                                      quantileValues[0,0] * C.omc_dcpd_output_to_darmin1_tf * 1e-12,
                                                      quantileValues[0,2] * C.omc_dcpd_output_to_darmin1_tf * 1e-12),
                                                      usetex=True)

s3.errorbar(frequencies, abs(processed_optical_response_tf/MAP_optical_response_tf), yerr=processed_optical_response_unc, fmt='.',label='meas. / MCMC')
#s3.errorbar(frequencies, abs(processed_optical_response_tf/model_optical_response_tf), yerr=processed_optical_response_unc, fmt='.', label='2021 Model')
s3.set_ylabel('|meas./model|')
s3.set_xscale('log')
s3.legend(ncol=1)
s3.grid(which='major',color='black')
s3.grid(which='minor',ls='--')
s3.set_xlim(plotFreqRange)
s3.xaxis.set_major_locator(tck.LogLocator(base=10))
s3.set_ylim([0.90,1.10])
s3.yaxis.set_major_locator(tck.MultipleLocator(0.02))
s3.yaxis.set_minor_locator(tck.MultipleLocator(0.005))
s3.set_title('$f_{{cc}}$ = %.4g${}_{-%.3g}^{+%.3g}$ Hz, $\\tau_C$ = %.2f${}_{-%.3g}^{+%.3g}$ $\mu$s'
          %(MAP[1],quantileValues[1,0],quantileValues[1,2],
            MAP[4],quantileValues[4,0],quantileValues[4,2]),usetex=True)

s4.errorbar(frequencies, np.angle(processed_optical_response_tf/MAP_optical_response_tf, deg=True), yerr=processed_optical_response_unc*180.0/np.pi, fmt='.')
#s4.errorbar(frequencies, np.angle(processed_optical_response_tf/model_optical_response_tf, deg=True), yerr=processed_optical_response_unc*180.0/np.pi, fmt='.')
s4.set_xlabel('Frequency (Hz)')
s4.set_ylabel('Phase diff. (meas./model)')
s4.set_xscale('log')
s4.grid(which='major',color='black')
s4.grid(which='minor',ls='--')
s4.set_xlim(plotFreqRange)
s4.xaxis.set_major_locator(tck.LogLocator(base=10))
s4.set_ylim([-10,5])
s4.yaxis.set_major_locator(tck.MultipleLocator(2))
s4.yaxis.set_minor_locator(tck.MultipleLocator(0.5))

if np.imag(MAP[2]) == 0:
    s4.set_title('$f_s$ = %.4g${}_{-%.3g}^{+%.3g}$ Hz, $Q_s$ = %.4g${}_{-%.3g}^{+%.3g}$'
          %(MAP[2],quantileValues[2,0],quantileValues[2,2],
            1.0/MAP[3],1.0/quantileValues[3,0],1.0/quantileValues[3,0]),usetex=True)
else:
    s4.set_title('$f_s$ = i%.4g${}_{-%.3g}^{+%.3g}$ Hz, $Q_s$ = %.4g${}_{-%.3g}^{+%.3g}$'
          %(np.imag(MAP[2]),quantileValues[2,0],quantileValues[2,2],
            1.0/MAP[3],1.0/quantileValues[3,0],1.0/quantileValues[3,0]),usetex=True)

plt.suptitle(IFO + ' sensing function measurement: ' + measDate,fontsize='x-large')
plt.tight_layout(rect=[0, 0.03, 1, 0.95])

if printFigs:
    thisfig = fignames(resultsDir+figTag+'_mcmcModel_vs_measurement.pdf')
    allfigures.append(thisfig)
    plt.savefig(thisfig.filename,bbox_inches='tight')

import pydarm
import numpy as np
import nds2
import os
import sys
from scipy import signal
from scipy.signal import freqz, welch, csd
from numpy.random import randn
from numpy import pi, exp, convolve


try:
    IFO = os.environ['IFO']
except:
    print('Environment does not define IFO variable, i.e. IFO=L1')
    sys.exit()


DEFAULT_NDS = 'l1nds0'
DEFAULT_NDS_PORT = 8088
#DEFAULT_NDS = 'nds.ligo.caltech.edu'
#DEFAULT_NDS_PORT = 31200

KAPPA_CHANNEL_LIST = [IFO+':CAL-CS_TDEP_KAPPA_C_OUTPUT',
                      IFO+':CAL-CS_TDEP_F_C_OUTPUT',
                      IFO+':CAL-CS_TDEP_KAPPA_UIM_OUTPUT',
                      IFO+':CAL-CS_TDEP_KAPPA_PUM_OUTPUT',
                      IFO+':CAL-CS_TDEP_KAPPA_TST_OUTPUT']

DEFAULT_CONFIG_NAME = 'pydarm_'+IFO+'.ini'


def get_kappas(gps_time):
    conn = nds2.connection(DEFAULT_NDS,DEFAULT_NDS_PORT)
    start = int(gps_time)
    end = start + 1
    kappa_data = conn.fetch(start,end,[IFO+':CAL-CS_TDEP_KAPPA_C_OUTPUT',
                        IFO+':CAL-CS_TDEP_F_C_OUTPUT',
                        IFO+':CAL-CS_TDEP_KAPPA_UIM_OUTPUT',
                        IFO+':CAL-CS_TDEP_KAPPA_PUM_OUTPUT',
                        IFO+':CAL-CS_TDEP_KAPPA_TST_OUTPUT'])
    kappa_dict = {'c':kappa_data[0].data[0],
        'f_c':kappa_data[1].data[0],
        'uim':kappa_data[2].data[0],
        'pum':kappa_data[3].data[0],
        'tst':kappa_data[4].data[0]}
    return kappa_dict


def apply_kappa_to_model(kappas,model):
    print('Applying kappa_c = ' + str(kappas['c']))
    model.sensing.coupled_cavity_optical_gain = model.sensing.coupled_cavity_optical_gain * kappas['c']
    print('Replacing cavity pole with ' + str(kappas['f_c']))
    model.sensing.coupled_cavity_pole_frequency = kappas['f_c']
    try:
        print('Applying kappa_uim = ' + str(kappas['uim']))
        model.actuation.xarm.uim_npa = model.actuation.xarm.uim_npa * kappas['uim']
    except:
        print("Warning: Did not update X UIM actuation")
    try:
        print('Applying kappa_uim = ' + str(kappas['uim']))
        model.actuation.yarm.uim_npa = model.actuation.yarm.uim_npa * kappas['uim']
    except:
        print("Warning: Did not update Y UIM actuation")
    try:
        print('Applying kappa_pum = ' + str(kappas['pum']))
        model.actuation.xarm.pum_npa = model.actuation.xarm.pum_npa * kappas['pum']
    except:
        print("Warning: Did not update X PUM actuation")
    try:
        print('Applying kappa_pum = ' + str(kappas['pum']))
        model.actuation.yarm.pum_npa = model.actuation.yarm.pum_npa * kappas['pum']
    except:
        print("Warning: Did not update Y PUM actuation")
    try:
        print('Applying kappa_tst = ' + str(kappas['tst']))
        model.actuation.xarm.tst_npv2 = model.actuation.xarm.tst_npv2 * kappas['tst']
    except:
        print("Warning: Did not update X TST actuation")
    try:
        print('Applying kappa_tst = ' + str(kappas['tst']))
        model.actuation.yarm.tst_npv2 = model.actuation.yarm.tst_npv2 * kappas['tst']
    except:
        print("Warning: Did not update Y TST actuation")
    return model


def apply_kappa_to_calcs(gps_time,config_dir,config_name=DEFAULT_CONFIG_NAME):
    kappas = get_kappas(gps_time)
    calcs_model = pydarm.calcs.CALCSModel(config_dir+'/'+config_name)
    return apply_kappa_to_model(kappas,calcs_model)

def apply_kappa_to_report(gps_time,config_dir,config_name=DEFAULT_CONFIG_NAME):
    kappas = get_kappas(gps_time)
    darm_model = pydarm.darm.DARMModel(config_dir+'/'+config_name)
    return apply_kappa_to_model(kappas,darm_model)

def apply_kappa_to_FIRgen(gps_time,config_dir,config_name=DEFAULT_CONFIG_NAME):
    kappas = get_kappas(gps_time)
    FIR_model = pydarm.fir.FIRFilterFileGeneration(config_dir+'/'+config_name)
    return apply_kappa_to_model(kappas,FIR_model)

def tfe(x, y, *args, **kwargs):
    #Code shamelessly stolen from Gabriele
    """
    tfe(x, y, *args, **kwargs)

    Estimate transfer function from x to y, see scipy.signals.csd for calling convention

    Returns tf, fr, cohe
    """
    fr,cxy = csd(x, y, *args, **kwargs)
    fr,px = welch(x, *args, **kwargs)
    fr,py = welch(y, *args, **kwargs)
    return cxy / px, fr, abs(cxy)**2/px/py


def interpolate_FIR_filter(npz_file_name,filter_name,freq_requested,sample_rate):
    half_sample_rate = int(sample_rate/2)
    filters = np.load(npz_file_name)
    filters[filter_name].shape
    temp_freq = np.linspace(0,half_sample_rate,half_sample_rate*10+1)
    w,h = freqz(filters[filter_name],worN=2*pi*temp_freq/int(sample_rate))
    temp_filter = h*exp(2j*pi*temp_freq*(filters[filter_name].shape[0]/2-1)/int(sample_rate))

    x = randn(sample_rate*100)
    y = convolve(x, filters[filter_name],mode='same')

    tf, freq_vector, c = tfe(x,y, nperseg=10*sample_rate, noverlap=5*sample_rate, fs=sample_rate)
    final_tf_interp = np.interp(freq_requested, freq_vector, tf)

    return final_tf_interp

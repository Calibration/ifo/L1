import pydarm
import numpy as np
import matplotlib.pyplot as plt


config = '../../pydarm_L1.ini'

config = '/ligo/groups/cal/L1/reports/20230913T185524Z/pydarm_L1.ini'
D = pydarm.darm.DARMModel(config)
ff = np.array([283.41,283.31])

C_response = D.sensing.compute_sensing(ff)
A_response = D.actuation.compute_actuation(ff)
D_response = D.digital.compute_response(ff)

sensing_suppression = C_response/(1+C_response*A_response*D_response)

print(sensing_suppression)
print("Ratio = ")
print(abs(sensing_suppression[0]/sensing_suppression[1]))


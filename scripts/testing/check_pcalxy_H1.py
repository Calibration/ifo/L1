import pydarm
import numpy as np
import matplotlib.pyplot as plt


config = '../../../../H1/arx/reports/20231027T203619Z/pydarm_H1.ini'
D = pydarm.darm.DARMModel(config)
ff = np.array([284.01,283.91])

C_response = D.sensing.compute_sensing(ff)
A_response = D.actuation.compute_actuation(ff)
D_response = D.digital.compute_response(ff)
sensing_suppression = C_response/(1+C_response*A_response*D_response)

print(sensing_suppression)
print("Ratio = ")
print(abs(sensing_suppression[0]/sensing_suppression[1]))


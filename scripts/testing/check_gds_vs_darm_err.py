#!/usr/bin/env python3

import pydarm
import numpy as np
import matplotlib.pyplot as plt
import pydarmtools

fh = open('./GDS_over_DARM_ERR_DBL.txt')
lines = fh.readlines()
fh.close()
freq = []
gds_over_darm_err_complex = []
for line in lines:
   freq.append(float(line.split()[0]))
   gds_over_darm_err_complex.append(float(line.split()[1]) + 1j*float(line.split()[2]))

freq = np.array(freq)
gds_over_darm_err_complex = np.array(gds_over_darm_err_complex)

g_over_d_corrected = []
for f,c in zip(freq,gds_over_darm_err_complex):
    phase_advance = np.exp(-2*np.pi*1j/16384.0*f)
    g_over_d_corrected.append(c*phase_advance)

g_over_d_corrected = np.array(g_over_d_corrected)


gps_time = 1373182698
config_name = 'pydarm_L1_site.ini'
config_dir = '/ligo/groups/cal/L1/reports/20230502T224106Z'

darm_model = pydarmtools.apply_kappa_to_report(gps_time,config_dir,config_name)

R_model = darm_model.compute_response_function(freq)

#Reading off 2023-05-25 07:53:56 to 2023-05-25 09:35:30 UTC, median values
monitor_freq = np.array([16.3, 33.93,54.17, 78.23,101.63, 283.41, 434.9, 1083.1])
monitor_violin_mag = np.array([1.020262,0.988486,0.982736,0.9862,0.998877, 1.004644, 0.999517, 0.998406])
monitor_violin_pha = np.array([-2.345188, -1.241855, -0.2345534, 0.6802748, 0.8713653, 0.3368919, 0.6438163, 1.892379])

plt.figure()
plt.semilogx(freq,np.abs(R_model/g_over_d_corrected/3994.5),label='R/(GDS/DARM_ERR)')
plt.semilogx(monitor_freq,monitor_violin_mag,label='Monitor PCAL/GDS')
plt.legend()
plt.title('Response function')
plt.xlabel('Freq (Hz)')
plt.ylabel('Mag ratio')
plt.xlim([10,2000])
plt.ylim([0.95,1.05])

plt.figure()
plt.semilogx(freq,np.angle(R_model/g_over_d_corrected,deg=True),label='R/(GDS/DARM_ERR)')
plt.semilogx(monitor_freq,monitor_violin_pha,label='Monitor PCAL/GDS')
plt.legend()
plt.title('Response function')
plt.xlabel('Freq (Hz)')
plt.ylabel('Phase diff')
plt.xlim([10,2000])
plt.ylim([-5,5])
plt.show()


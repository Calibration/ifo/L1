#!/usr/bin/env python3
import pydarm
import numpy as np
import matplotlib.pyplot as plt

import foton
class FilterModule():
    def __init__(self, filterBank = None, filterModule = None, FM = None):
        if (filterBank != None) and (filterModule != None) and (FM != None):
            ff = foton.FilterFile('/opt/rtcds/llo/l1/chans/'+filterBank+'.txt')
            self.moduleFM = ff[filterModule][FM]
    def fotonResp(self,f):
        return self.moduleFM.freqresp(f)



config = '../../pydarm_L1.ini'
D = pydarm.darm.DARMModel(config)
ff = np.logspace(-2,4,num=10000)
A = pydarm.actuation.ActuationModel(config,measurement='actuation_x_arm')

uim,pum,tst = A.matlab_force2length_response('/ligo/svncommon/CalSVN/aligocalibration/trunk/Common/pyDARM/matlab_scripts/20230202_L1_EX_O3_susdata.mat',ff)

pum_drivealign_toESD = FilterModule('L1SUSETMX','ETMX_L2_DRIVEALIGN_L2L',0)
pum_foton_toESD = pum_drivealign_toESD.fotonResp(ff)

pum_foton_x_model = pum_foton_toESD*pum

plt.figure()
plt.loglog(ff,abs(pum_foton_x_model))
plt.loglog(ff,abs(tst))

#!/usr/bin/env python3


import matplotlib.pyplot as plt
import numpy as np
import pydarm
import pydarmtools
from gwpy.timeseries import TimeSeries
from scipy import signal
from astropy import units as u
from scipy.signal import freqz, welch, csd
from numpy.random import randn
from numpy import pi, exp, convolve
from pydarm.utils import digital_delay_filter
from pydarm.digital import daqdownsamplingfilters
#FIR filter loading

#filters = np.load('./gstlal_compute_strain_C00_filters_L1.npz')
#filters_fixed = np.load('/data/joseph.betzwieser/cal/L1/reports2/20230502T224106Z/gstlal_compute_strain_C00_filters_L1.npz')

old_filter_file = './gstlal_compute_strain_C00_filters_L1.npz'
latest_filter_file = '/data/joseph.betzwieser/cal/L1/reports2/20230502T224106Z/gstlal_compute_strain_C00_filters_L1.npz'


freq = np.linspace(0,2048,32769)
freq16384 = np.linspace(0,8192,131073)


invsens_old_FIR = pydarmtools.interpolate_FIR_filter(old_filter_file,'res_corr_noccpole_filter',freq,16384)
tst_old_FIR = pydarmtools.interpolate_FIR_filter(old_filter_file,'TST_corr_filter',freq,2048)
pum_old_FIR = pydarmtools.interpolate_FIR_filter(old_filter_file,'PUM_corr_filter',freq,2048)
uim_old_FIR = pydarmtools.interpolate_FIR_filter(old_filter_file,'UIM_corr_filter',freq,2048)


invsens_latest_FIR = pydarmtools.interpolate_FIR_filter(latest_filter_file,'res_corr_noccpole_filter',freq,16384)
tst_latest_FIR = pydarmtools.interpolate_FIR_filter(latest_filter_file,'TST_corr_filter',freq,2048)
pum_latest_FIR = pydarmtools.interpolate_FIR_filter(latest_filter_file,'PUM_corr_filter',freq,2048)
uim_latest_FIR = pydarmtools.interpolate_FIR_filter(latest_filter_file,'UIM_corr_filter',freq,2048)

gpstime = 1373846652
config_name = 'pydarm_L1_site.ini'
config_dir = '/ligo/groups/cal/L1/reports/20230502T224106Z'
config = config_dir + '/' + config_name


#Pydarm model generation
darm_model = pydarmtools.apply_kappa_to_report(gpstime,config_dir,config_name)
calcs_model = pydarmtools.apply_kappa_to_calcs(gpstime,config_dir,config_name)
calcs_model = pydarm.calcs.CALCSModel(config_dir+'/'+config_name)
#darm_model_no_kappa = pydarm.darm.DARMModel(config)

R_model = darm_model.compute_response_function(freq)
C_model = darm_model.sensing.compute_sensing(freq)
A_model = darm_model.actuation.compute_actuation(freq)
D_model = darm_model.digital.compute_response(freq)

TST_model = darm_model.actuation.stage_super_actuator(freq,stage='TST')
PUM_model = darm_model.actuation.stage_super_actuator(freq,stage='PUM')
UIM_model = darm_model.actuation.stage_super_actuator(freq,stage='UIM')

R_individual = 1.0/C_model + (TST_model+PUM_model+UIM_model) * D_model
invsens_corr = (1.0/C_model)*invsens_latest_FIR/invsens_old_FIR
TST_corr = TST_model*tst_latest_FIR/tst_old_FIR
PUM_corr = PUM_model*pum_latest_FIR/pum_old_FIR
UIM_corr = UIM_model*uim_latest_FIR/uim_old_FIR
A_corr = TST_corr + PUM_corr + UIM_corr
R_corr = invsens_corr + A_corr * D_model


#Grab live data

start = gpstime
end = start + 480
darm_err_ts = TimeSeries.get('L1:CAL-DARM_ERR_DBL_DQ',start,end)
tst_ts = TimeSeries.get('L1:CAL-DELTAL_CTRL_TST_DBL_DQ',start,end)
pum_ts = TimeSeries.get('L1:CAL-DELTAL_CTRL_PUM_DBL_DQ',start,end)
uim_ts = TimeSeries.get('L1:CAL-DELTAL_CTRL_UIM_DBL_DQ',start,end)
act_ts = TimeSeries.get('L1:CAL-DELTAL_CTRL_DBL_DQ',start,end)
res_ts = TimeSeries.get('L1:CAL-DELTAL_RESIDUAL_DBL_DQ',start,end)
gds_ts = TimeSeries.get('L1:GDS-CALIB_STRAIN',start,end)

fft_length = 16
overlap = 8


darm_err_fft = darm_err_ts.average_fft(fft_length,overlap,window='hann')
tst_fft = tst_ts.average_fft(fft_length,overlap,window='hann')
pum_fft = pum_ts.average_fft(fft_length,overlap,window='hann')
uim_fft = uim_ts.average_fft(fft_length,overlap,window='hann')
act_fft = act_ts.average_fft(fft_length,overlap,window='hann')
res_fft = res_ts.average_fft(fft_length,overlap,window='hann')
gds_fft = gds_ts.average_fft(fft_length,overlap,window='hann')

darm_err_asd = darm_err_ts.asd(fft_length,overlap,window='hann')
res_asd = res_ts.asd(fft_length,overlap,window='hann')
gds_asd = gds_ts.asd(fft_length,overlap,window='hann')

darm_err_fft2048 = darm_err_fft[0:32769]
gds_fft2048 = gds_fft[0:32769]

g_over_d = gds_fft2048/darm_err_fft2048
g_over_d_corrected = []
for f,c in zip(freq,g_over_d):
    phase_advance = np.exp(-2*np.pi*1j/16384.0*f)
    g_over_d_corrected.append(c*phase_advance*darm_model.sensing.mean_arm_length())
g_over_d_corrected = np.array(g_over_d_corrected)


omc_to_calcs_response = (
            signal.dfreqresp(digital_delay_filter(1, 16384),
                             2.0*np.pi*freq/16384)[1])

daqdownsampling = signal.dfreqresp(
                daqdownsamplingfilters(16384, 4096, 'biquad', 'v3'),
                2.0*np.pi*freq/16384)[1]


tst_pydarm_gds_corr = calcs_model.stage_super_actuator(freq,'TST')*omc_to_calcs_response*daqdownsampling*D_model
pum_pydarm_gds_corr = calcs_model.stage_super_actuator(freq,'PUM')*omc_to_calcs_response*daqdownsampling*D_model
uim_pydarm_gds_corr = calcs_model.stage_super_actuator(freq,'UIM')*omc_to_calcs_response*daqdownsampling*D_model




fig,axs = plt.subplots(2,2)
axs[0,0].loglog(freq,abs(R_model),label='R')
axs[0,0].loglog(freq,abs(R_individual),label='1/C + AD')
axs[0,0].loglog(freq,abs(R_corr),label='R corrected')
axs[0,0].loglog(freq,abs(g_over_d_corrected),label='GDS/DARM_ERR')
axs[0,0].legend()
fig.suptitle('Complete R vs 1/C + AD')
axs[0,0].set(xlabel='Freq (Hz)')
axs[0,0].set(ylabel='Mag ratio')


axs[1,0].semilogx(freq,np.angle(R_model,deg=True),label='R')
axs[1,0].semilogx(freq,np.angle(R_individual,deg=True),label='1/C + AD')
axs[1,0].semilogx(freq,np.angle(R_corr,deg=True),label='R corrected')
axs[1,0].semilogx(freq,np.angle(g_over_d_corrected,deg=True),label='GDS/DARM_ERR')
axs[1,0].legend()
axs[1,0].set(xlabel='Freq (Hz)')
axs[1,0].set(ylabel='Phase diff')

axs[0,1].semilogx(freq,abs(R_model/g_over_d_corrected),label='R/GDS/DARM_ERR')
axs[0,1].semilogx(freq,abs(R_individual/R_model),label='(1/C + AD)/R')
axs[0,1].semilogx(freq,abs(R_corr/R_model),label='R_corr/R')
axs[0,1].legend()
axs[0,1].set(xlabel='Freq (Hz)')
axs[0,1].set(ylabel='Mag ratio')
axs[0,1].set_ylim(0.5,1.5)
#axs[0,1].set_xlim(100,8000)

axs[1,1].semilogx(freq,np.angle(R_model/g_over_d_corrected,deg=True),label='R/GDS/DARM_ERR')
axs[1,1].semilogx(freq,np.angle(R_individual/R_model,deg=True),label='(1/C + AD)/R')
axs[1,1].semilogx(freq,np.angle(R_corr/R_model,deg=True),label='R_corr/R')

axs[1,1].legend()
axs[1,1].set(xlabel='Freq (Hz)')
axs[1,1].set(ylabel='Phase diff')
axs[1,1].set_ylim(-5,5)
#axs[1,1].set_xlim(100,8000)

plt.show()



#TST
fig,axs = plt.subplots(2,2)
axs[0,0].loglog(freq,abs(tst_fft/darm_err_fft2048),label='TST/DARM_ERR')
axs[0,0].loglog(freq,abs(tst_pydarm_gds_corr),label='Pydarm FE TST act')
axs[0,0].legend()
fig.suptitle('Measured FE TST TF vs Pydarm predictions')
axs[0,0].set(xlabel='Freq (Hz)')
axs[0,0].set(ylabel='Mag ratio')


axs[1,0].semilogx(freq,np.angle(tst_fft/darm_err_fft2048,deg=True),label='TST/DARM_ERR')
axs[1,0].semilogx(freq,np.angle(tst_pydarm_gds_corr,deg=True),label='Pydarm FE TST act')
axs[1,0].legend()
axs[1,0].set(xlabel='Freq (Hz)')
axs[1,0].set(ylabel='Phase diff')

axs[0,1].semilogx(freq,abs(tst_pydarm_gds_corr/(tst_fft/darm_err_fft2048)),label='Pydarm/TST/DARM_ERR')
axs[0,1].legend()
axs[0,1].set(xlabel='Freq (Hz)')
axs[0,1].set(ylabel='Mag ratio')
axs[0,1].set_ylim(0.5,1.5)
#axs[0,1].set_xlim(100,8000)

axs[1,1].semilogx(freq,np.angle(tst_pydarm_gds_corr/(tst_fft/darm_err_fft2048),deg=True),label='Pydarm/TST/DARM_ERR')
axs[1,1].legend()
axs[1,1].set(xlabel='Freq (Hz)')
axs[1,1].set(ylabel='Phase diff')
axs[1,1].set_ylim(-5,5)
#axs[1,1].set_xlim(100,8000)

plt.show()

#PUM

fig,axs = plt.subplots(2,2)
axs[0,0].loglog(freq,abs(pum_fft/darm_err_fft2048),label='PUM/DARM_ERR')
axs[0,0].loglog(freq,abs(pum_pydarm_gds_corr),label='Pydarm FE PUM act')
axs[0,0].legend()
fig.suptitle('Measured FE PUM TF vs Pydarm predictions')
axs[0,0].set(xlabel='Freq (Hz)')
axs[0,0].set(ylabel='Mag ratio')


axs[1,0].semilogx(freq,np.angle(pum_fft/darm_err_fft2048,deg=True),label='PUM/DARM_ERR')
axs[1,0].semilogx(freq,np.angle(pum_pydarm_gds_corr,deg=True),label='Pydarm FE PUM act')
axs[1,0].legend()
axs[1,0].set(xlabel='Freq (Hz)')
axs[1,0].set(ylabel='Phase diff')

axs[0,1].semilogx(freq,abs(pum_pydarm_gds_corr/(pum_fft/darm_err_fft2048)),label='Pydarm/PUM/DARM_ERR')
axs[0,1].legend()
axs[0,1].set(xlabel='Freq (Hz)')
axs[0,1].set(ylabel='Mag ratio')
axs[0,1].set_ylim(0.5,1.5)
#axs[0,1].set_xlim(100,8000)

axs[1,1].semilogx(freq,np.angle(pum_pydarm_gds_corr/(pum_fft/darm_err_fft2048),deg=True),label='Pydarm/PUM/DARM_ERR')
axs[1,1].legend()
axs[1,1].set(xlabel='Freq (Hz)')
axs[1,1].set(ylabel='Phase diff')
axs[1,1].set_ylim(-5,5)
#axs[1,1].set_xlim(100,8000)

plt.show()

#UIM

fig,axs = plt.subplots(2,2)
axs[0,0].loglog(freq,abs(uim_fft/darm_err_fft2048),label='UIM/DARM_ERR')
axs[0,0].loglog(freq,abs(uim_pydarm_gds_corr),label='Pydarm FE UIM act')
axs[0,0].legend()
fig.suptitle('Measured FE UIM TF vs Pydarm predictions')
axs[0,0].set(xlabel='Freq (Hz)')
axs[0,0].set(ylabel='Mag ratio')


axs[1,0].semilogx(freq,np.angle(uim_fft/darm_err_fft2048,deg=True),label='UIM/DARM_ERR')
axs[1,0].semilogx(freq,np.angle(uim_pydarm_gds_corr,deg=True),label='Pydarm FE UIM act')
axs[1,0].legend()
axs[1,0].set(xlabel='Freq (Hz)')
axs[1,0].set(ylabel='Phase diff')

axs[0,1].semilogx(freq,abs(uim_pydarm_gds_corr/(uim_fft/darm_err_fft2048)),label='Pydarm/UIM/DARM_ERR')
axs[0,1].legend()
axs[0,1].set(xlabel='Freq (Hz)')
axs[0,1].set(ylabel='Mag ratio')
axs[0,1].set_ylim(0.5,1.5)
#axs[0,1].set_xlim(100,8000)

axs[1,1].semilogx(freq,np.angle(uim_pydarm_gds_corr/(uim_fft/darm_err_fft2048),deg=True),label='Pydarm/UIM/DARM_ERR')
axs[1,1].legend()
axs[1,1].set(xlabel='Freq (Hz)')
axs[1,1].set(ylabel='Phase diff')
axs[1,1].set_ylim(-5,5)
#axs[1,1].set_xlim(100,8000)

plt.show()


pum_test = []
for v in pum_ts.value:
    pum_test.append(np.float64(v))

pum_test_array = np.array(pum_test)
darm_err_test = []
for v in darm_err_ts.value:
    darm_err_test.append(np.float(v))
darm_err_test_array = np.array(darm_err_test)

tf_test,freq_test, c = pydarmtools.tfe(darm_err_test_array,pum_test_array)

plt.figure()
plt.loglog(freq_test,abs(tf_test))
plt.show()

plt.figure()
plt.loglog(freq,abs(pum_fft),label='PUM')
plt.loglog(freq,abs(tst_fft),label='TST')
plt.legend()
plt.figure()
plt.loglog(freq,abs(darm_err_fft2048))
plt.show()

plt.figure()
plt.loglog(freq,abs(daqdownsampling))
plt.show(plt.legend())

#!/usr/bin/env python3

import pydarm
import numpy as np
from cdsutils import nds


config = '../../pydarm_L1.ini'
calcs = pydarm.calcs.CALCSModel(config)
epics_records = calcs.compute_epics_records(endstation=True)



start = 1366044604
end = start + 120

site = 'l1'

conn = nds.connection(site+'nds0',8088)

channels = []
for line in ['1','2','3']:
    for iq in ['I','Q']:
        channels.append(site.upper() + ':CAL-CS_TDEP_SUS_LINE' + line + '_SUS_DEMOD_'+iq+'_OUTPUT')
        channels.append(site.upper() + ':CAL-CS_TDEP_SUS_LINE' + line + '_ERR_DEMOD_'+iq+'_OUTPUT')
for line in ['1','2']:
    for iq in ['I','Q']:
        channels.append(site.upper() + ':CAL-CS_TDEP_PCAL_LINE' + line + '_PCAL_DEMOD_'+iq+'_OUTPUT')
        channels.append(site.upper() + ':CAL-CS_TDEP_PCAL_LINE' + line + '_ERR_DEMOD_'+iq+'_OUTPUT')

data = conn.fetch(start,end,channels)
IQ_dict = {}
for count, chan in enumerate(channels):
    IQ_dict[chan] = np.mean(data[count].data)

#Now we begin the math section

pcal_line2_pcal_demod = IQ_dict[site.upper() + ':CAL-CS_TDEP_PCAL_LINE2_PCAL_DEMOD_I_OUTPUT'] - 1j*IQ_dict[site.upper() + ':CAL-CS_TDEP_PCAL_LINE2_PCAL_DEMOD_Q_OUTPUT']
pcal_line2_err_demod = IQ_dict[site.upper() + ':CAL-CS_TDEP_PCAL_LINE2_ERR_DEMOD_I_OUTPUT'] - 1j*IQ_dict[site.upper() + ':CAL-CS_TDEP_PCAL_LINE2_ERR_DEMOD_Q_OUTPUT']

darm_over_pcal = pcal_line2_err_demod/pcal_line2_pcal_demod

darm_pcal_corr = 5.26646e-06+1j*3.10158e-7

corrected_darm_over_pcal = darm_over_pcal/darm_pcal_corr

one_over_C = 1.0/corrected_darm_over_pcal
one_over_inv_C = 1.0/one_over_C

nocavpole_corr = -5.36999e+11+1j*1.29304e+11

corrected_C = one_over_inv_C/nocavpole_corr

kappa_C = (np.real(corrected_C)**2 + np.imag(corrected_C)**2)/np.real(corrected_C)
f_c = -np.real(corrected_C)/np.imag(corrected_C)*434.9

print(kappa_C)
print(f_c)





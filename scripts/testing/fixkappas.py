import pydarm
import numpy as np
import nds2
from matplotlib import pyplot as plt


IFO = 'H1'

DEFAULT_NDS = 'nds.ligo-wa.caltech.edu'
DEFAULT_NDS_PORT = 31200

KAPPA_CHANNEL_LIST = [IFO+':CAL-CS_TDEP_KAPPA_C_OUTPUT',
                      IFO+':CAL-CS_TDEP_F_C_OUTPUT',
                      IFO+':CAL-CS_TDEP_KAPPA_UIM_OUTPUT',
                      IFO+':CAL-CS_TDEP_KAPPA_PUM_OUTPUT',
                      IFO+':CAL-CS_TDEP_KAPPA_TST_OUTPUT',
                      IFO+':GDS-CALIB_KAPPA_C',
                      IFO+':GDS-CALIB_F_CC',
                      IFO+':GDS-CALIB_KAPPA_UIM_REAL',
                      IFO+':GDS-CALIB_KAPPA_PUM_REAL',
                      IFO+':GDS-CALIB_KAPPA_TST_REAL']

DEFAULT_CONFIG_NAME = 'pydarm_'+IFO+'.ini'
DEFAULT_CONFIG_DIR = '/ligo/groups/cal/H1/reports/20230621T211522Z/'
DEFAULT_CONFIG_NAME = 'pydarm_H1_site.ini'

def get_kappas(gps_time):
    conn = nds2.connection(DEFAULT_NDS,DEFAULT_NDS_PORT)
    start = int(gps_time)
    end = start + 1
    kappa_data = conn.fetch(start,end,KAPPA_CHANNEL_LIST)
    kappa_dict = {'c':kappa_data[0].data[0],
        'f_c':kappa_data[1].data[0],
        'uim':1.025,
        'pum':kappa_data[3].data[0],
        'tst':kappa_data[4].data[0],
        'gds_c':kappa_data[5].data[0],
        'gds_f_c':kappa_data[6].data[0],
        'gds_uim':kappa_data[7].data[0],
        'gds_pum':kappa_data[8].data[0],
        'gds_tst':kappa_data[9].data[0]}
    return kappa_dict

def get_true_model(gps_time,config_dir=DEFAULT_CONFIG_DIR,config_name=DEFAULT_CONFIG_NAME):
    kappas = get_kappas(gps_time)
    model = pydarm.darm.DARMModel(config_dir+'/'+config_name)
    model.sensing.coupled_cavity_optical_gain = model.sensing.coupled_cavity_optical_gain * kappas['c']
    model.sensing.coupled_cavity_pole_frequency = kappas['f_c']
    model.actuation.xarm.uim_npa = model.actuation.xarm.uim_npa * kappas['uim']
    model.actuation.xarm.pum_npa = model.actuation.xarm.pum_npa * kappas['pum']
    model.actuation.xarm.tst_npv2 = model.actuation.xarm.tst_npv2 * kappas['tst']
    return model

def get_applied_model(gps_time,config_dir=DEFAULT_CONFIG_DIR,config_name=DEFAULT_CONFIG_NAME):
    kappas = get_kappas(gps_time)
    model = pydarm.darm.DARMModel(config_dir+'/'+config_name)
    model.sensing.coupled_cavity_optical_gain = model.sensing.coupled_cavity_optical_gain * kappas['gds_c']
    model.sensing.coupled_cavity_pole_frequency = kappas['gds_f_c']
    model.actuation.xarm.uim_npa = model.actuation.xarm.uim_npa * kappas['gds_uim']
    model.actuation.xarm.pum_npa = model.actuation.xarm.pum_npa * kappas['gds_pum']
    model.actuation.xarm.tst_npv2 = model.actuation.xarm.tst_npv2 * kappas['gds_tst']
    model.actuation.xarm.tst_driver_uncompensated_p_ul = [31.549e3]
    model.actuation.xarm.tst_driver_uncompensated_p_ll = [26.699e3]
    model.actuation.xarm.tst_driver_uncompensated_p_ur = [26.617e3]
    model.actuation.xarm.tst_driver_uncompensated_p_lr = [31.618e3]
    return model

frequencies_gpr = np.logspace(np.log10(1),np.log10(5000),100)

gps_time = 1374369018

true_model = get_true_model(gps_time)
applied_model = get_applied_model(gps_time)
R_true = true_model.compute_response_function(frequencies_gpr)
R_applied = applied_model.compute_response_function(frequencies_gpr)

systematic = R_true/R_applied

fig,axs = plt.subplots(2,1)

axs[0].semilogx(frequencies_gpr,abs(systematic))
axs[0].set(xlabel='Freq [Hz]')
axs[0].set(ylabel='Ratio [mag]')
fig.suptitle('True R / Applied R')
axs[1].semilogx(frequencies_gpr,np.angle(systematic,deg=True))
axs[1].set(xlabel='Freq [Hz]')
axs[1].set(ylabel='Phase [deg]')

plt.show()

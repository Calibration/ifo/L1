import pydarm
import numpy as np
import matplotlib.pyplot as plt


config = '../ifo/L1/pydarm_L1.ini'
D = pydarm.darm.DARMModel(config)
ff = np.logspace(-1,3,num=10000)
Ax = pydarm.actuation.ActuationModel(config,measurement='actuation_x_arm')
Ay = pydarm.actuation.ActuationModel(config,measurement='actuation_y_arm')
cal = pydarm.calcs.CALCSModel(config)


act_uim = D.actuation.stage_super_actuator(ff,stage='UIM')
cal_uim = cal.stage_super_actuator(ff,stage='UIM')


plt.figure()
plt.semilogx(ff,np.angle(cal_uim,deg=True),label='cal uim')
plt.semilogx(ff,np.angle(act_uim,deg=True),label='act uim')
plt.legend()
plt.show()

cal_x_uim_only = cal.compute_actuation_single_stage(ff,'x','UIM')
act_x_uim_only = Ax.compute_actuation_single_stage(ff,'UIM')
cal_y_uim_only = cal.compute_actuation_single_stage(ff,'y','UIM')
act_y_uim_only = Ay.compute_actuation_single_stage(ff,'UIM')

plt.figure()
plt.loglog(ff,abs(cal_x_uim_only),label='cal x uim')
plt.loglog(ff,abs(act_x_uim_only),label='act x uim')
plt.loglog(ff,abs(cal_y_uim_only),label='cal y uim')
plt.loglog(ff,abs(act_y_uim_only),label='act y uim')
plt.legend()
plt.show()

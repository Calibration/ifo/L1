#!/usr/bin/env python3

import numpy as np
import gwpy.timeseries as gwpyTS

import cdsutils


startTime = 1367097393+1
endTime = 1367097426+1
duration = endTime - startTime


readback_channels = ['L1:CAL-PCALY_RX_PD_OUT_DQ','L1:LSC-DARM_IN1_DQ']

cdsdata = cdsutils.getdata(readback_channels,duration,startTime)

data = gwpyTS.TimeSeriesDict()
for channum in range(len(readback_channels)):
    data[readback_channels[channum]] = gwpyTS.TimeSeries( cdsdata[channum].data,
        channel     = cdsdata[channum].name,
        sample_rate = cdsdata[channum].sample_rate,
        t0          = cdsdata[channum].start_time,
        )

singleMEasurementTime = 6.451612903225806
singleMEasurementTime = np.float64(singleMEasurementTime)
sampleRate = int(data['L1:CAL-PCALY_RX_PD_OUT_DQ'].sample_rate.value)
fft_length = singleMEasurementTime#int(np.round(singleMEasurementTime))#duration / 5.0#
avs = 5
samplesInSinleMeasurement = int(round(singleMEasurementTime * sampleRate, 0))#int(duration/5*data['L1:CAL-PCALY_RX_PD_OUT_DQ'].sample_rate.value)#
print(samplesInSinleMeasurement)

A = data['L1:CAL-PCALY_RX_PD_OUT_DQ'][0: int(avs*samplesInSinleMeasurement)]
B = data['L1:LSC-DARM_IN1_DQ'][0: int(avs*samplesInSinleMeasurement)]


Afft = A.average_fft(fft_length, 0, window='hann')
Bfft = B.average_fft(fft_length, 0, window='hann')

cohArray = B.coherence(A , fftlength = fft_length, overlap = 0, window='hann')

f = 12.4

index = int(round((f - Bfft.f0.value) / Bfft.df.value))
print(index)
print(f / Afft.df)
print(np.complex128( Bfft[index] / Afft[index] ))
print(20 * np.log10(np.complex128( Bfft[index] / Afft[index] )))
print(np.array([np.float64(cohArray[index-2]), np.float64(cohArray[index-1]), np.float64(cohArray[index]), np.float64(cohArray[index+1]), np.float64(cohArray[index+2])]))

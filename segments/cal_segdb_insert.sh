# Permalink for calibration flag segdb insert executable
EXECUTABLE=https://git.ligo.org/Calibration/calmonitor/-/raw/7bfa87b94bcfdcf988cf5c7532c109103fbeeaf2/bin/cal_flag_segdb_insert
# Segment database URL
SEGDB_URL=https://segments.ligo.org
# Set IFO for this push
export IFO=L1
# Set observing run to determine sub-directory path for segment files
OBSRUN=O4
# Get date-time string
DATETIME=$(TZ=UTC date '+%Y.%m.%d-%H.%M.%S')

# Make sure current repo state is committed
git commit -m "State of repo at $DATETIME UTC before segdb push" 

# From here on, quit if something fails
set -e

# Download the executable file for doing the segdb pushing
wget $EXECUTABLE

# Push to segdb for DCS-CAL_C00_BAD flag
python3 ./cal_flag_segdb_insert --known-segments-file $OBSRUN/$IFO-DCS-CAL_C00_BAD-known.txt --active-segments-file $OBSRUN/$IFO-DCS-CAL_C00_BAD-active.txt --flag-name DCS-CAL_C00_BAD --segment-database $SEGDB_URL --append

# Push to segdb for DCS-CAL_LL_UNCERTAINTY_BAD flag
python3 ./cal_flag_segdb_insert --known-segments-file $OBSRUN/$IFO-DCS-CAL_LL_UNCERTAINTY_BAD-known.txt --active-segments-file $OBSRUN/$IFO-DCS-CAL_LL_UNCERTAINTY_BAD-active.txt --flag-name DCS-CAL_LL_UNCERTAINTY_BAD --segment-database $SEGDB_URL --append

# Push to segdb for DCS-CAL_C00_60HZ_HARM_SUB_ON flag
python3 ./cal_flag_segdb_insert --known-segments-file $OBSRUN/$IFO-DCS-CAL_C00_60HZ_HARM_SUB_ON-known.txt --active-segments-file $OBSRUN/$IFO-DCS-CAL_C00_60HZ_HARM_SUB_ON-active.txt --flag-name DCS-CAL_C00_60HZ_HARM_SUB_ON --segment-database $SEGDB_URL

# Push to segdb for DCS-CAL_C01_60HZ_HARM_SUB_ON flag
python3 ./cal_flag_segdb_insert --known-segments-file $OBSRUN/$IFO-DCS-CAL_C01_60HZ_HARM_SUB_ON-known.txt --active-segments-file $OBSRUN/$IFO-DCS-CAL_C01_60HZ_HARM_SUB_ON-active.txt --flag-name DCS-CAL_C01_60HZ_HARM_SUB_ON --segment-database $SEGDB_URL

# Set tag for this push in git
git tag -a segdb-push-$DATETIME-UTC -m "Segdb push occurred at $DATETIME UTC"
git push origin segdb-push-$DATETIME-UTC
